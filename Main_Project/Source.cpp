/*
����: Source.cpp
������� �� ������ ��������
�����: �������� �.�.
�������: ������� �4
*/
#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <locale.h>
#include <math.h>
// ������ ���
int dec_to_bin(int a, int b[]);
// �������������� ��������
void invert(int b[], int i);
//������������ ����� ������� ����
void print_bin(int b[], int i);
// �������������� ���
void dopcode(int b[], int i);
int main(void) {
	int number;
	int bin[40];
	setlocale(LC_ALL, "Rus");
	printf("������� �����:");
	scanf("%i", &number);
	if (number == 0) {
		printf("���� �� ����� 0:\n");
		printf("� ������ ����: 0000\n� �������� ����: 0000\n� �������������� ����: 0000\n ");
		printf("���� �� ����� -0:\n");
		printf("� ������ ����: 1000\n� �������� ����: 1111\n� �������������� ����: ---\n ");
		system("pause");
		return 0;
	}
	int i = dec_to_bin(number,&bin[0]);
	printf("� ������ ����: ");
	print_bin(&bin[0], i);
	printf("� �������� ����: ");
	if (number < 0) {
		invert(&bin[0], i);
		print_bin(&bin[0], i);
		printf("� �������������� ���� : ");
		dopcode(&bin[0], i);
		print_bin(&bin[0], i);
		system("pause");
		return 0;
	}
	print_bin(&bin[0], i);
	printf("� �������������� ���� : ");
	print_bin(&bin[0], i);
	system("pause");
	return 0;
}
int dec_to_bin(int a,int b[]) {
	int i;
	int d = 0;
	if (a < 0)
		d = 1;
	a = fabs(a);
	for (i = 0;i < 40; i++) {
		b[i] = a % 2;
		if ((a = a / 2) == 0) {
			if (d == 1) {
				for (i++;; i++) {
					if ((i + 1) % 4 != 0)
						b[i] = 0;
					else {
						b[i] = 1;
						break;
					}
				}
			}
			else {
				for (i++;; i++) {
					if ((i+1) % 4 != 0)
						b[i] = 0;
					else {
						b[i] = 0;
						break;
					}
				}
			}
			b[i+1] = 99;
			break;
		}
	}
	return i;
}
void invert(int b[], int i) {
	int a = i;
	for (i; i >= 0; i--) {
		if (i == a)
			continue;
		if (b[i] == 0)
			b[i] = 1;
		else
			b[i] = 0;
	}
}
void print_bin(int b[], int i) {
	for (i; i >= 0; i--) {
		printf("%i", b[i]);
		if (!(i % 4))
			printf(" ");
	}
	printf("\n");
}
void dopcode(int b[], int i) {
	i = 0;
	b[i] += 1;
	for (i = 0;;i++) {
		if (b[i] == 2) {
			b[i] = 0;
			b[i + 1] = b[i + 1] + 1;
			if (b[i + 1] == 100) {
				b[i + 1] = 1;
				return;
			}
		}
		if (b[i] == 99)
			return;
	}
}